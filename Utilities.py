import cv2
from matplotlib import pyplot as plt
from sklearn.mixture.gmm import GMM
from sklearn.cluster import KMeans
import numpy
from enum import Enum

class Benchmark(Enum):
    gmm = "gmm"
    kmeans = "kmeans"

def preprocess(I):
    I = cv2.bilateralFilter(I, 5, 50, 50)
    return I

def postprocess(I):
    kern = cv2.getStructuringElement(cv2.MORPH_RECT,(5,5))
    I = cv2.morphologyEx(I, cv2.MORPH_CLOSE, kern)
    return I

def plot_hist(I):
    color = ('b','g','r')
    for i,col in enumerate(color):
        histr = cv2.calcHist([I],[i],None,[256],[0,256])
        plt.plot(histr,color = col)
        plt.xlim([0,256])
    plt.show()

def fit_benchmark(I, btype = Benchmark.gmm):

    g = None
    if btype == Benchmark.gmm:
        g = GMM(n_components = 2, n_init = 50)
    else:
        g = KMeans(n_clusters = 2, n_init = 50)
    I_array = numpy.reshape(I, (I.shape[0] * I.shape[1], 3))
    g.fit(I_array)
    clusters = g.predict(I_array)

    ones = len(numpy.where(clusters == 1)[0])
    zeros = len(numpy.where(clusters == 0)[0])

    clusters = numpy.reshape(clusters, (I.shape[0], I.shape[1]))
    img = None

    if zeros >= ones:
        img = numpy.zeros((I.shape[0], I.shape[1], 3))
        img[clusters == 1] = [255, 255, 255]
    else:
        img = numpy.ones((I.shape[0], I.shape[1], 3)) * 255
        img[clusters == 1] = [0, 0, 0]

    img.astype(numpy.uint8)
    return img

def count_unique_pixels(I):
    uniq = set()
    for i in xrange(I.shape[0]):
        for j in xrange(I.shape[1]):
            print I[i,j]
            uniq.add(tuple(I[i,j]))
    return uniq
