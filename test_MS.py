from AmbrosioTortorelliMinimizer import *
import Utilities

f = "317080.jpg"
img = cv2.imread('BerkeleySubset/test/'+f,1)
result, edges = [], []
for c in cv2.split(img):
	solver = AmbrosioTortorelliMinimizer(c, alpha = 10, beta = 0.05,
	epsilon = 0.01)

	f, v = solver.minimize()
	result.append(f)
	edges.append(v)

img_res = cv2.merge(result)
edges = np.maximum(*edges)

cv2.imshow("result",img_res)
cv2.imshow("edges",edges)
cv2.waitKey(0)
cv2.destroyAllWindows()

print len(Utilities.count_unique_pixels(img_res))