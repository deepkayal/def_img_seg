import cv2
import Utilities
import subprocess
import os
from multiprocessing import Process

def process_one(I, filename, dir_to_write, btype):

    os.system("mkdir -p %s"%dir_to_write)

    I = Utilities.preprocess(I)
    G = Utilities.fit_benchmark(I, btype)
    G = Utilities.postprocess(G)

    filename = filename.split('.')[0]
    cv2.imwrite(dir_to_write + filename + ".jpg", G)


def process_all(btype):
    dir_to_write = "BerkeleySubset/out_%s/"%(str(btype))
    dir_to_read = "BerkeleySubset/test/"
    files = subprocess.Popen(["ls", dir_to_read], stdout = subprocess.PIPE).stdout.read()[0:-1].split('\n')

    count = 0
    for i in xrange(0, len(files), 5):
        print "File: %s, %s of %s"%(dir_to_read + files[i], count + 1, len(files))
        print "File: %s, %s of %s"%(dir_to_read + files[i + 1], count + 2, len(files))
        print "File: %s, %s of %s"%(dir_to_read + files[i + 2], count + 3, len(files))
        print "File: %s, %s of %s"%(dir_to_read + files[i + 3], count + 4, len(files))
        print "File: %s, %s of %s"%(dir_to_read + files[i + 4], count + 5, len(files))



        I1 = cv2.imread(dir_to_read + files[i], 1)
        p1 = Process(target = process_one, args = (I1, files[i], dir_to_write, btype))

        I2 = cv2.imread(dir_to_read + files[i + 1], 1)
        p2 = Process(target = process_one, args = (I2, files[i + 1], dir_to_write, btype))

        I3 = cv2.imread(dir_to_read + files[i + 2], 1)
        p3 = Process(target = process_one, args = (I3, files[i + 2], dir_to_write, btype))

        I4 = cv2.imread(dir_to_read + files[i + 3], 1)
        p4 = Process(target = process_one, args = (I4, files[i + 3], dir_to_write, btype))

        I5 = cv2.imread(dir_to_read + files[i + 4], 1)
        p5 = Process(target = process_one, args = (I5, files[i + 4], dir_to_write, btype))

        p1.start()
        p2.start()
        p3.start()
        p4.start()
        p5.start()

        p1.join()
        p2.join()
        p3.join()
        p4.join()
        p5.join()

        count += 5

def main():
    print "GMM"
    process_all(Utilities.Benchmark.gmm)
    print "Kmeans"
    process_all(Utilities.Benchmark.kmeans)
main()
