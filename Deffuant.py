import random
import numpy
import copy

class Deffuant:

    def __init__(self, epsilon, mu = 0.5, tol = 0.00001, max_iter = 10000):

        self.epsilon = epsilon
        self.mu = mu
        self.tol = tol
        self.max_iter = max_iter

    def __get_opposite(self, perm_idx):

        original_idx = [0]*len(perm_idx)
        for i in xrange(len(perm_idx)):
            original_idx[perm_idx[i]] = i
        return original_idx

    def __transform_from_list_to_matrix_coordinates(self, p, shape):
        x = p/shape[1]
        y = (p - shape[1]*x)
        return (x,y)

    def __calculate_normalized_distance(self, x, y, shape):
        x = self.__transform_from_list_to_matrix_coordinates(x, shape)
        y = self.__transform_from_list_to_matrix_coordinates(y, shape)
        dist = float(abs(x[0] - y[0]) + abs(x[1] - y[1])) - 1.
        norm = dist / (shape[0] + shape[1])
        return norm

    def __get_float(self, X):
        X = X.astype(float)
        X = X/255
        return X

    def __get_uint8(self, X):
        X = X*255
        X = X.astype(numpy.uint8)
        return X

    def __get_reshaped(self, X):
        X_shape = X.shape
        ndim = len(X_shape)
        if ndim != 3:
            raise Exception("Not an image!")
        if X.dtype != float:
            raise Exception("Convert image to float!")
        X = numpy.reshape(X, (X_shape[0] * X_shape[1],3))

        return (X_shape, X)

    def get_clustered(self, X, weighted = True, debug = False):
        """
        :param X: image of type float [0-1]
        :return: clustered image of type float [0-1]
        """

        dt = str(X.dtype)
        if dt == "uint8":
            X = self.__get_float(X)

        (X_shape, X) = self.__get_reshaped(X)

        Y = copy.deepcopy(X)
        original = None
        count = 0
        perm_idx = None
        original_idx = None

        while count < self.max_iter:

            count = count+1

            stop = len(X)
            if stop%2 != 0:
                stop -= 1

            for i in xrange(0, stop, 2):
                diff = abs(X[i]-X[i+1])

                weight = None
                if original_idx == None or not weighted:
                    weight = 1
                else:
                    d = self.__calculate_normalized_distance(original_idx[i], original_idx[i+1], (X_shape[0],X_shape[1]))
                    weight = (1-d)

                if diff[0] < self.epsilon and diff[1] < self.epsilon and diff[2] < self.epsilon:
                    for idx in xrange(3):
                        Y[i,idx] = X[i,idx] + self.mu * (X[i+1,idx] - X[i,idx]) * weight
                        Y[i+1,idx] = X[i+1,idx] + self.mu * (X[i,idx] - X[i+1,idx]) * weight

            change = numpy.abs(Y - X)

            if debug:
                print "On iteration %s, max change is %s"%(count,numpy.max(change))

            if count != 1 and numpy.max(change) <= self.tol:
                break
            else:
                if original_idx == None:
                    original = copy.deepcopy(Y)
                else:
                    Y = Y[original_idx]
                    original = copy.deepcopy(Y)
                perm_idx = random.sample(range(len(Y)), len(Y))
                original_idx = self.__get_opposite(perm_idx)
                Y = Y[perm_idx]
                X = copy.deepcopy(Y)

        X = numpy.reshape(original, (X_shape))

        if dt == "uint8":
            X = self.__get_uint8(X)

        return X
