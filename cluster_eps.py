import cv2
from Deffuant import Deffuant
import Utilities
import subprocess
import os
from matplotlib import pyplot as plt

files = subprocess.Popen(["ls", "BerkeleySubset/test/"], stdout = subprocess.PIPE).stdout.read()[0:-1].split('\n')

epsilons = [0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6]

count=0
for f in files:
    print "File: %s, %s of %s"%(f,count+1,len(files))
    count_unique = []
    pixels = None

    for eps in epsilons:
        print "\t for epsilon = %s"%eps
        I=cv2.imread('BerkeleySubset/test/'+f,1)

        deffuant = Deffuant(epsilon = eps, max_iter = 200)
        I = deffuant.get_clustered(I, weighted = True, debug = False)

        pixels = Utilities.count_unique_pixels(I)
        count_unique.append(len(pixels))

    plt.plot(epsilons,count_unique,'-x')

    count += 1

plt.grid()
#plt.show()
plt.savefig('eps-clusters.png')