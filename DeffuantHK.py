import numpy
import copy

class DeffuantHK:

    def __init__(self, epsilon, mu = 0.5, tol = 0.00001, max_iter = 10000):

        self.epsilon = epsilon
        self.mu = mu
        self.tol = tol
        self.max_iter = max_iter

    def __transform_from_list_to_matrix_coordinates(self, p, shape):
        x = p/shape[1]
        y = (p - shape[1]*x)
        return (x,y)

    def __transform_matrix_coordinates_to_list(self, x, y, shape):
        return x*shape[1] + y

    def __get_average(self, arr, coordinates, shape, N):
        x = coordinates[0]
        y = coordinates[1]
        val = arr[self.__transform_matrix_coordinates_to_list(x, y, shape)]
        avg = []
        for i in xrange(x - N, x + N + 1):
            for j in xrange(y - N, y + N + 1):
                if i >= 0 and j >= 0 and i < shape[0] and j < shape[1]:
                    val_point = arr[self.__transform_matrix_coordinates_to_list(i, j, shape)]
                    diff = abs(val - val_point)
                    if diff[0] < self.epsilon and diff[1] < self.epsilon and diff[2] < self.epsilon:
                        avg.append(val_point)
        return numpy.mean(avg, axis = 0)

    def __get_reshaped(self, X):
        X_shape = X.shape
        ndim = len(X_shape)
        if ndim != 3:
            raise Exception("Not an image!")
        if X.dtype != float:
            raise Exception("Convert image to float!")
        X = numpy.reshape(X, (X_shape[0] * X_shape[1],3))

        return (X_shape, X)

    def __get_float(self, X):
        X = X.astype(float)
        X = X/255
        return X

    def __get_uint8(self, X):
        X = X*255
        X = X.astype(numpy.uint8)
        return X

    def get_clustered(self, X, neighborhood = 1, debug = False):
        """
        :param X: image of type float [0-1]
        :return: clustered image of type float [0-1]
        """

        dt = str(X.dtype)
        if dt == "uint8":
            X = self.__get_float(X)

        (X_shape, X) = self.__get_reshaped(X)

        Y = copy.deepcopy(X)
        count = 0
        idx = numpy.arange(X_shape[0] * X_shape[1])

        while count < self.max_iter:

            count = count+1

            stop = len(X)
            if stop%2 != 0:
                stop -= 1

            for i in xrange(0, stop, 2):
                diff = abs(X[idx[i]]-X[idx[i+1]])

                (x1,y1) = self.__transform_from_list_to_matrix_coordinates(idx[i], X_shape)
                (x2,y2) = self.__transform_from_list_to_matrix_coordinates(idx[i + 1], X_shape)

                if diff[0] < self.epsilon and diff[1] < self.epsilon and diff[2] < self.epsilon:
                    avg1 = self.__get_average(X, (x1,y1), X_shape, neighborhood)
                    avg2 = self.__get_average(X, (x2,y2), X_shape, neighborhood)

                    for color in xrange(3):
                        Y[idx[i], color] = avg1[color] + self.mu * (avg2[color] - avg1[color])
                        Y[idx[i+1], color] = avg2[color] + self.mu * (avg1[color] - avg2[color])

            change = numpy.abs(Y - X)

            if debug:
                print "On iteration %s, max change is %s"%(count,numpy.max(change))

            if count != 1 and numpy.max(change) <= self.tol:
                break
            else:
                numpy.random.shuffle(idx)
                X = copy.deepcopy(Y)

        X = numpy.reshape(X, (X_shape))

        if dt == "uint8":
            X = self.__get_uint8(X)

        return X
