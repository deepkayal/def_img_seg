from Deffuant import Deffuant
from DeffuantHK import DeffuantHK
from enum import Enum
import Utilities
import numpy

class Type(Enum):
    deffuant = 1
    deffuant_hk = 2

class DeffuantIterative:
    def __init__(self, clusters = 2, model = Type.deffuant):
        self.clusters = clusters
        self.model = model

    def __count_unique_pixels(self, I):
        uniq = dict()
        for i in xrange(I.shape[0]):
            for j in xrange(I.shape[1]):
                if tuple(I[i,j]) not in uniq:
                    uniq[tuple(I[i,j])] = 0
                uniq[tuple(I[i,j])] += 1
        return uniq

    def __change_pixels(self, X, change_dict):
        for i in xrange(X.shape[0]):
            for j in xrange(X.shape[1]):
                if tuple(X[i,j]) in change_dict:
                    X[i,j] = change_dict[tuple(X[i,j])]
        return X

    def __combine_if_small_area(self, uniq, X):
        total_area = X.shape[0] * X.shape[1]
        keys = uniq.keys()
        small_keys = []
        large_keys = []

        for i in xrange(len(keys)):
            if uniq[keys[i]] < 0.01 * total_area:
                small_keys.append(keys[i])
            else:
                large_keys.append(keys[i])

        change_dict = {}
        for small_key in small_keys:
            d = []
            for large_key in large_keys:
                d.append(numpy.sqrt(numpy.sum(numpy.subtract(large_key, small_key)**2)))
            idx = numpy.where(d == numpy.min(d))[0][0]
            change_dict[tuple(small_key)] = tuple(large_keys[idx])

        return self.__change_pixels(X, change_dict)

    def __get_color_labels(self, num):
        start = 0
        end = 255
        delta = 255 / (num - 1)
        colors = [[0,0,0]]
        for n in xrange(num - 2):
            color = start + (n+1) * delta
            colors.append([color, color, color])
        colors.append([255,255,255])
        return colors

    def __change_to_label(self, X):
        uniq_colors = self.__count_unique_pixels(X)
        colors = uniq_colors.keys()
        change_dict = {}
        if len(uniq_colors) == 2:
            if uniq_colors[colors[0]] >= uniq_colors[colors[1]]:
                change_dict[colors[0]] = [0,0,0]
                change_dict[colors[1]] = [255,255,255]
            else:
                change_dict[colors[1]] = [0,0,0]
                change_dict[colors[0]] = [255,255,255]
        else:
            color_labels = self.__get_color_labels(len(colors))
            for i in xrange(len(colors)):
                change_dict[tuple(colors[i])] = color_labels[i]

        return self.__change_pixels(X, change_dict)

    def get_clustered(self, X, debug = False):
        epsilon = 0.1
        delta_epsilon = (epsilon/10.)

        Model_type = None
        if self.model == Type.deffuant:
            if debug:
                print "Using deffuant model"
            Model_type = Deffuant
        elif self.model == Type.deffuant_hk:
            if debug:
                print "Using deffuant-HK hybrid model"
            Model_type = DeffuantHK
        else:
            raise Exception("Wrong model type!")

        deffuant = Model_type(epsilon = epsilon)

        while True:
            X = deffuant.get_clustered(X, debug = debug)

            uniq = self.__count_unique_pixels(X)
            num_unique = len(uniq)
            if num_unique <= self.clusters:
                break

            if debug:
                print "Unique pixels = %s for epsilon = %s"%(num_unique, epsilon)

            if num_unique <= self.clusters + 5:
                epsilon += delta_epsilon/4.
            else:
                epsilon += delta_epsilon
                X = Utilities.preprocess(X)
            deffuant = Model_type(epsilon = epsilon)

            if num_unique <= 100:
                X = self.__combine_if_small_area(uniq, X)

        X = self.__change_to_label(X)

        return X