
import numpy
import random
import copy
import matplotlib.pyplot as plt

mu=0.5
epsilon=[0.05,0.1,0.2,0.5]

subplotcount=1
for eps in epsilon:
	print eps
	count=0
	x = numpy.sort(numpy.random.uniform(0,1,1000))
	y = copy.deepcopy(x)
	while True:
		count=count+1
		for i in xrange(0,len(x),2):
			diff = abs(x[i]-x[i+1])
			if diff < eps:
				y[i]=x[i]+mu*(x[i+1]-x[i])
				y[i+1]=x[i+1]+mu*(x[i]-x[i+1])
		p=numpy.abs(y-x)
		if max(p) <= 0.000001:
			break
		else:
			numpy.random.shuffle(y)
			x=copy.deepcopy(y)
		plt.subplot(2,2,subplotcount),plt.plot([count]*1000,x,'b.'),plt.title('epsilon = %s'%eps)
		if subplotcount==1 or subplotcount==3:
			plt.subplot(2,2,subplotcount),plt.ylabel('Opinion')
		if subplotcount==3 or subplotcount==4:
			plt.subplot(2,2,subplotcount),plt.xlabel('Timestamp')
	subplotcount+=1
plt.rcParams["figure.figsize"]=[10,10]
plt.savefig('deffuant.jpg',dpi=200)
