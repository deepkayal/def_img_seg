from skimage.segmentation import slic
import subprocess
from skimage.util import img_as_float
from skimage import io
import os
import numpy as np
import cv2

dir_to_write = "BerkeleySubset/out_%s/"%("SLIC")
dir_to_read = "BerkeleySubset/test/"
files = subprocess.Popen(["ls", dir_to_read], stdout = subprocess.PIPE).stdout.read()[0:-1].split('\n')

os.system("mkdir -p %s"%dir_to_write)

for i in xrange(0, len(files)):

    # load the image and convert it to a floating point data type
    image = img_as_float(io.imread(dir_to_read + files[i]))

    segments = slic(image, n_segments = 2, sigma = 5, compactness = 1)

    img = np.zeros((segments.shape[0], segments.shape[1], 3))
    if np.count_nonzero(segments) < (segments.shape[0] * segments.shape[1] - np.count_nonzero(segments)):
        img[segments == 1] = [255, 255, 255]
    else:
        img[segments == 0] = [255, 255, 255]

    img.astype(np.uint8)

    cv2.imwrite(dir_to_write + files[i], img)