import cv2
import subprocess
import numpy

def labeled_image_to_bw(I):
    for i in xrange(I.shape[0]):
        for j in xrange(I.shape[1]):
            if sum(I[i,j]) != 0:
                I[i,j] = [255, 255, 255]
    return I

def counts(I, J):
    result = {'TP':0,'TN':0,'FP':0,'FN':0}
    for i in xrange(J.shape[0]):
        for j in xrange(J.shape[1]):

            if I[i,j,0] == 255 and I[i,j,1] == 255 and I[i,j,2] == 255 and I[i,j,0] == J[i,j,0] and I[i,j,1] == J[i,j,1] and I[i,j,2] == J[i,j,2]:
                result['TP'] += 1

            if I[i,j,0] == 0 and I[i,j,1] == 0 and I[i,j,2] == 0 and I[i,j,0] == J[i,j,0] and I[i,j,1] == J[i,j,1] and I[i,j,2] == J[i,j,2]:
                result['TN'] += 1

            if I[i,j,0] == 0 and I[i,j,1] == 0 and I[i,j,2] == 0 and I[i,j,0] != J[i,j,0] and I[i,j,1] != J[i,j,1] and I[i,j,2] != J[i,j,2]:
                result['FP'] += 1

            if I[i,j,0] == 255 and I[i,j,1] == 255 and I[i,j,2] == 255 and I[i,j,0] != J[i,j,0] and I[i,j,1] != J[i,j,1] and I[i,j,2] != J[i,j,2]:
                result['FN'] += 1
    return result

def process_one(I, J):
    I  = labeled_image_to_bw(I)
    pix_counts = counts(I, J)

    recall = pix_counts['TP'] / float(pix_counts['TP'] + pix_counts['FN'])
    fallout = pix_counts['FP'] / float(pix_counts['FP'] + pix_counts['TN'])
    accuracy = (pix_counts['TP'] + pix_counts['TN']) / float(pix_counts['TP'] + pix_counts['FP'] + pix_counts['TN'] + pix_counts['FN'])

    return (recall, fallout, accuracy)

def process_all(dir_to_read, dir_labeled = "BerkeleySubset/labeled/"):

    files = subprocess.Popen(["ls", dir_labeled], stdout = subprocess.PIPE).stdout.read()[0:-1].split('\n')

    count = 0

    recall_all = []
    fallout_all = []
    acc_all = []

    for f in files:

        f2 = f.split('.')[0]
        f2 = f2 + ".jpg"

        #print "File: %s, %s of %s"%(dir_labeled + f, count + 1, len(files))
        #print "And file: %s, %s of %s"%(dir_to_read + f2, count + 1, len(files))

        I = cv2.imread(dir_labeled + f, 1)
        J = cv2.imread(dir_to_read + f2, 1)
        (recall, fallout, acc) = process_one(I, J)

        #print "Recall: %s, Fallout: %s, Accuracy: %s"%(recall, fallout, acc)
        recall_all.append(recall)
        fallout_all.append(fallout)
        acc_all.append(acc)

        count += 1

    return (recall_all, fallout_all, acc_all)

def main():
    (recall_all, fallout_all, acc_all) = process_all("BerkeleySubset/out_def/")
    print "Def - Recall: %s, Fallout: %s, Acc: %s"%(numpy.mean(recall_all), numpy.mean(fallout_all), numpy.mean(acc_all))

    (recall_all, fallout_all, acc_all) = process_all("BerkeleySubset/out_kmeans/")
    print "Kmeans - Recall: %s, Fallout: %s, Acc: %s"%(numpy.mean(recall_all), numpy.mean(fallout_all), numpy.mean(acc_all))

    (recall_all, fallout_all, acc_all) = process_all("BerkeleySubset/out_SLIC/")
    print "SLIC - Recall: %s, Fallout: %s, Acc: %s"%(numpy.mean(recall_all), numpy.mean(fallout_all), numpy.mean(acc_all))
main()
