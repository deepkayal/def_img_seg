import cv2
import copy
from DeffuantIterative import DeffuantIterative
import Utilities
import time

f = "3096.jpg"

print "File: %s"%(f)
I = cv2.imread('BerkeleySubset/test/'+f,1)
I = Utilities.preprocess(I)
orig = copy.deepcopy(I)
I = cv2.resize(I, dsize = (0,0), fx = 0.5, fy = 0.5, interpolation = cv2.INTER_NEAREST)

start_def = time.time()
deffuant = DeffuantIterative()
I = deffuant.get_clustered(I, debug = True)
end_def = time.time() - start_def
I = cv2.resize(I, dsize = (0,0), fx = 2, fy = 2, interpolation = cv2.INTER_NEAREST)

start_bench = time.time()
G = Utilities.fit_benchmark(orig, btype = Utilities.Benchmark.kmeans)
end_bench = time.time() - start_bench

I = Utilities.postprocess(I)
G = Utilities.postprocess(G)

fname = f.split('.')[0]

cv2.imshow("deffuant",I)
cv2.imshow("Kmeans",G)
cv2.waitKey(0)
cv2.destroyAllWindows()

print "deffuant time taken = %s"%end_def
print "benchmark time taken = %s"%end_bench